#include <iostream>
#include <string>
#include <list>

using namespace std;

class file{
    private:
        string name;
        bool is_dir {0};
        file* link;
        list<file*> contents;
    public:
        file(string name, bool is_dir){
            this->name = name;
            this->is_dir = is_dir;
            this->link = NULL;
        }
        
        file(string name, bool is_dir, file* link){
            this->name = name;
            this->is_dir = is_dir;
            this->link = link;
        }
        
        void setlink(file* link){
            this->link = link;
        }
        
        void ls(){
            // cout<<"Directory listing for: "<<this->name<<endl;
            for(auto f=contents.begin(); f!=contents.end(); f++){
                cout<<(*f)->name<<endl;
            }
        }
        
        void mkdir(string dir_name){
            for(auto f=contents.begin(); f!=contents.end(); f++){
                if((*f)->name==dir_name){
                    cout<<dir_name<<" already exists"<<endl;
                    return;
                }
            }
            
            file* d = new file(dir_name,true);
            d->setlink(d);
            file* c = new file(".", true, d);
            file* u = new file("..", true, this);
            (d->contents).push_back(c);
            (d->contents).push_back(u);
            contents.push_back(d);
        }
        
        void touch(string file_name){
            for(auto f=contents.begin(); f!=contents.end(); f++){
                if((*f)->name==file_name){
                    cout<<file_name<<" already exists"<<endl;
                    return;
                }
            }
            
            file* f = new file(file_name,false);
            contents.push_back(f);
        }
        
        file* cd(string dir_name){
            for(auto f=contents.begin(); f!=contents.end(); f++){
                if((*f)->name==dir_name){
                    if((*f)->is_dir){
                        return (*f)->link;
                    }else{
                        cout<<dir_name<<" is a file"<<endl;
                    }
                }
            }
            cout<<"No such directory "<<dir_name<<endl;
            return this;
        }
        
        void pwd(){
            file* tmp_pwd = this;
            list<string> path_to_root;
            cout<<"\\";
            while(tmp_pwd->name!="root"){
                path_to_root.push_front(tmp_pwd->name);
                tmp_pwd=tmp_pwd->cd("..");
            }
            
            for(auto f=path_to_root.begin(); f!=path_to_root.end(); f++){
                cout<<(*f)<<"\\";
            }
            cout<<endl;
        }
        
        void tree(file* root, unsigned int level = 0){
            for(unsigned int l=0;l<level;l++){
                cout<<"  ";
            }
            cout<<"|-"<<root->name<<endl;
            
            for(auto f=root->contents.begin(); f!=root->contents.end(); f++){
                if((*f)->name=="." || (*f)->name==".."){
                    continue;
                }
                tree(*f,level+1);
            }
        }
};

int main()
{
    string cmd;
    file* pwd = new file("root", true);
    pwd->setlink(pwd);
    
    while(true){
        string arg;
        cout<<" $ ";
        cin>>cmd;
        
        if(cmd == "exit" || cmd == "quit"){
            return 0;
        }else if(cmd == "ls"){
            pwd->ls();
        }else if(cmd == "mkdir"){
            cin>>arg;
            pwd->mkdir(arg);
        }else if(cmd == "touch"){
            cin>>arg;
            pwd->touch(arg);
        }else if(cmd == "cd"){
            cin>>arg;
            pwd = pwd->cd(arg);
        }else if(cmd == "pwd"){
            pwd->pwd();
        }else if(cmd == "md"){
            cin>>arg;
            pwd->mkdir(arg);
            pwd=pwd->cd(arg);
        }else if(cmd == "tree"){
            pwd->tree(pwd, 0);
        }else{
            cout<<"Unknown command "<<cmd<<endl;
        }
    }
    
    return 0;
}
